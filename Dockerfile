FROM php:7.3-apache

# Install packages
RUN apt-get update && \
    apt-get install -y cron sudo \
    bash supervisor curl zip unzip xz-utils libxrender1 gnupg libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && php -m \
    && docker-php-ext-configure gd \
    && docker-php-ext-install mysqli gd opcache \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash && \
    apt-get install -y nodejs && \
    curl -o- -L https://yarnpkg.com/install.sh | bash -s -- && \
    ln -sfv /root/.yarn/bin/* /bin && \
    ln -sfv /root/.yarn/bin/node-gyp-bin/node-gyp /bin/node-gyp && \
    rm -rf /var/cache/apt && rm -rf /var/lib/apt && \
    curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin --filename=composer && \ 
    mkdir -p /run/php && \
    mkdir -p /var/www && \
    chown -R www-data:www-data /var/www /root

# Set upsteam repo
ENV GIT_REPO=https://github.com/osTicket/osTicket

# Scripts
COPY bin /bin
COPY conf/supervisord /etc/supervisor/conf.d/osticket.conf
COPY conf/msmtp /etc/msmtp.default

# Conf files
RUN touch /etc/msmtp /etc/osticket.secret.txt /etc/cron.d/osticket && \
    chown www-data:www-data /etc/msmtp /etc/osticket.secret.txt /etc/cron.d/osticket && \
    chown root:www-data /bin/vendor && chmod 770 /bin/vendor

# Deployment
RUN apt-get update && apt-get install -yf git
RUN git clone ${GIT_REPO} /var/www/tmp/
RUN php /var/www/tmp/manage.php deploy -v --setup /var/www/html/
RUN ls -l /var/www/html

RUN cp /var/www/html/include/ost-sampleconfig.php /var/www/html/include/ost-config.php
RUN chmod 0666 /var/www/html/include/ost-config.php

VOLUME /var/www
